//package com.crud.react.controler;
//
//import com.crud.react.model.ProductImage;
//import com.crud.react.service.ProductImageService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.util.List;
//
//@RestController
//@RequestMapping("/product-image")
//public class ProductImageControler {
//
//    @Autowired
//    ProductImageService productImageService;
//
//    @PostMapping(consumes = "multipart/form-data")
//    public ProductImage addImage(@RequestPart("Id") String productId, @RequestPart("file") MultipartFile multipartFile) {
//        return productImageService.addImage(Long.valueOf(productId), multipartFile);
//    }
//
//    @GetMapping
//    public List<ProductImage> getAll() {
//        return productImageService.findAll();
//    }
//}
