package com.crud.react.controler;

import com.crud.react.DTO.LoginDTO;
import com.crud.react.DTO.UserDTO;
import com.crud.react.model.Users;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.UsersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/users")
public class UsersControler {

    @Autowired
    private UsersService usersService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/sign-in")
    public CommonResponse<Map<String, Object>> login(@RequestBody LoginDTO loginDTO) {
        return ResponseHelper.ok(usersService.login(loginDTO));
    }

    @PostMapping("/sign-up")
    public CommonResponse<Users> addUsers(@RequestBody UserDTO userDTO) {
    return ResponseHelper.ok(usersService.addUsers(modelMapper.map(userDTO, Users.class)));
    }

    @GetMapping("/all")
    public CommonResponse<List<Users>> getAllUsers() {
        return ResponseHelper.ok(usersService.getAllUsers());
    }

    @GetMapping("/{Id}")
    public CommonResponse<Users> getUsers(@PathVariable("Id")Long Id) {
        return ResponseHelper.ok(usersService.getUsers(Id));
    }

    @PostMapping
    public CommonResponse<Users> addUsers(@RequestBody Users users) {
        return ResponseHelper.ok (usersService.addUsers(users));
    }
    @PutMapping("/{Id}")
    public CommonResponse<Users> editUsers(@PathVariable("Id") Long Id, @RequestBody Users users) {
        return ResponseHelper.ok (usersService.editUsers(Id, users.getEmail(), users.getPassword()));
    }

    @DeleteMapping("/{Id}")
    public void deleteUsersById(@PathVariable("Id") Long Id) {
        usersService.deleteUsersById(Id);
    }
}
