package com.crud.react.controler;

import com.crud.react.DTO.ProductDTO;
import com.crud.react.model.Product;
import com.crud.react.model.Users;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/product")
public class ProductControler {

    @Autowired
    private ProductService productService;

    @GetMapping("/all")
    public CommonResponse<List<Product>> getAllProduct() {
        return ResponseHelper.ok(productService.getAllProduct());
    }

    @GetMapping("/{Id}")
    public CommonResponse <Product> getProduct(@PathVariable("Id")Long Id) {
        return ResponseHelper.ok(productService.getProduct(Id));
    }

    @PostMapping(consumes = "multipart/form-data")
    public CommonResponse<Product> addProduct(@RequestPart("file") MultipartFile multipartFile, ProductDTO productDTO) {
        return ResponseHelper.ok(productService.addProduct(multipartFile, productDTO));
    }

    @PutMapping("/{Id}")
    public CommonResponse<Product> EditProduct(@PathVariable("Id") Long Id, @RequestBody Product product) {
        return ResponseHelper.ok (productService.EditProduct(Id, product.getGambar(), product.getNamaMakanans(), product.getDeskripsi(), product.getHarga()));
    }

    @DeleteMapping("/{Id}")
    public void deleteProductById(@PathVariable("Id") Long Id) {
        productService.deleteProductById(Id);
    }

}
