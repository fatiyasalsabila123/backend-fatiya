package com.crud.react.service;

import com.crud.react.DTO.ProductDTO;
import com.crud.react.model.Product;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ProductService {

    List<Product> getAllProduct();

    Product addProduct(MultipartFile multipartFile, ProductDTO productDTO);

    Product getProduct(Long Id);

    Product EditProduct(Long Id, String gambar, String namaMakanans, String deskripsi, Integer harga);

    void deleteProductById(Long Id);
}
