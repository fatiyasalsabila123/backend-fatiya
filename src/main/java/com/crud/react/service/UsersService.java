package com.crud.react.service;

import com.crud.react.DTO.LoginDTO;
import com.crud.react.model.Users;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

//userService
public interface UsersService {

    Map<String, Object> login(LoginDTO loginDTO);

    List<Users> getAllUsers();

    Users addUsers(Users users);

    Users getUsers(Long Id);

    Users editUsers(Long id, String email, String password);

//    void deleteUsersById(Long Id);

    @Transactional
    Users editUsers(Long id, Users users);

    Map<String, Boolean> deleteUsersById(Long id);
}



