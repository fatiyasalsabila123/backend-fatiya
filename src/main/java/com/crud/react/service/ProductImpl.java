package com.crud.react.service;

import com.crud.react.DTO.ProductDTO;
import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.Product;
//import com.crud.react.model.ProductImage;
import com.crud.react.repository.ProductRepository;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

@Service
public class ProductImpl implements ProductService{

    private static final String DOWNLOAD_URL= "https://firebasestorage.googleapis.com/v0/b/tugas-firebase-9dba4.appspot.com/o/%s?alt=media";


    @Autowired
    ProductRepository productRepository;

    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }

    @Override
    public Product addProduct(MultipartFile multipartFile, ProductDTO productDTO) {
        String url =imageConverter(multipartFile);
        Product product1 = new Product(url, productDTO.getNamaMakanans(),productDTO.getDeskripsi(), productDTO.getHarga());
        return productRepository.save(product1);
    }

    @Override
    public Product getProduct(Long Id) {
        return productRepository.findById(Id).orElseThrow(() -> new NotFoundException(("Id Tidak Di Temukan")));
    }

    @Override
    public Product EditProduct(Long id, String gambar, String namaMakanans, String deskripsi, Integer harga){
        Product product = productRepository.findById(id).get();
        product.setGambar(gambar);
        product.setNamaMakanans(namaMakanans);
        product.setDeskripsi(deskripsi);
        product.setHarga(harga);
        return productRepository.save(product);
    }

    @Override
    public void deleteProductById(Long Id) {
        productRepository.deleteById(Id);
    }

    private String imageConverter(MultipartFile multipartFile){
        try {
            String fileName = getExtenions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        }catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("Error Upload File");
        }
    }

    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)){
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blobId = BlobId.of("tugas-firebase-9dba4.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));
    }

    private String getExtenions(String fileName) {
        return fileName.split("\\.")[0];
    }
}
