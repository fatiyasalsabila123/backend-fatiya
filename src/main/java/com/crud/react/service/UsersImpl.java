package com.crud.react.service;

import com.crud.react.DTO.LoginDTO;
import com.crud.react.JWT.JWTProvider;
import com.crud.react.enumated.UserType;
import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.Users;
import com.crud.react.repository.UsersRepository;
import org.apache.juli.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UsersImpl implements UsersService {

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JWTProvider jwtProvider;

    @Autowired
    PasswordEncoder passwordEncoder;


    private String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        }catch (BadCredentialsException e) {
            throw new InternalErrorException("Email Or Pasword Not Found");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return jwtProvider.generateToken(userDetails);
    }

    @Override
    public Map<String, Object> login(LoginDTO loginDTO) {
        String token = authories(loginDTO.getEmail(), loginDTO.getPassword());
        Users users = usersRepository.findByEmail(loginDTO.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        response.put("expired", "15 menit");
        response.put("users", users);
        return response;
    }

    public List<Users> getAllUsers() {
            return usersRepository.findAll();
    }


    @Override
    public Users addUsers(Users users) {
        String email = users.getEmail();
        users.setPassword(passwordEncoder.encode(users.getPassword()));
        var validasi = usersRepository.findByEmail(email);
        if (users.getRole().name().equals("ADMIN"))
            users.setRole(UserType.ADMIN);
        else users.setRole(UserType.USER);
        if (validasi.isPresent()) {
            throw new InternalErrorException("Email Already Axist");
        }
        return usersRepository.save(users);
    }

    @Override
    public Users getUsers(Long Id) {
        return usersRepository.findById(Id).orElseThrow(() -> new NotFoundException(("Id tidak Di Temukan")));

    }

    @Override
    public Users editUsers(Long id, String email, String password){
        Users users = usersRepository.findById(id).get();
        users.setEmail(email);
        users.setPassword(password);
        return usersRepository.save(users);
    }

    @Transactional
    @Override
    public Users editUsers(Long id, Users users) {
        Users update = usersRepository.findById(id).orElseThrow(() -> new NotFoundException("Tidak Ada"));
        var validasi = usersRepository.findByEmail(users.getEmail());
        if (validasi.isPresent()) {
            throw new InternalErrorException("Maaf email sudah ada");
        }
        update.setEmail(users.getEmail());
        update.setPassword(users.getPassword());
        return update;

    }

    @Override
    public Map<String, Boolean> deleteUsersById(Long id) {
        try {
            usersRepository.deleteById(id);
            Map<String, Boolean> res = new HashMap<>();
            res.put("deleted", Boolean.TRUE);
            return res;
        } catch (Exception e) {
            throw new NotFoundException("Id Not Found");
        }
    }
}
