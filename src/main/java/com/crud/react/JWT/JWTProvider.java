package com.crud.react.JWT;

import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.TemporaryToken;
import com.crud.react.model.Users;
import com.crud.react.repository.TemporaryTokenRepository;
import com.crud.react.repository.UsersRepository;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JWTProvider {

    private static String seccetKey = "belajar spring";

    private static Integer expired = 900000;

    @Autowired
    public TemporaryTokenRepository temporaryTokenRepository;

    @Autowired
    private UsersRepository usersRepository;

    public String generateToken(UserDetails userDetails) {
        String token = UUID.randomUUID().toString().replace("_", "");
        Users users = usersRepository.findByEmail(userDetails.getUsername()).orElseThrow(() -> new NotFoundException("User Not Found Generate Token"));
        var chekingToken = temporaryTokenRepository.findByUserId(users.getId());
        if (chekingToken.isPresent()) temporaryTokenRepository.deleteById(chekingToken.get().getId());
        TemporaryToken temporaryToken = new TemporaryToken();
        temporaryToken.setToken(token);
        temporaryToken.setToken(token);
        temporaryToken.setExpiredDate(new Date(new Date().getTime() + expired));
        temporaryToken.setUserId(users.getId());
        temporaryTokenRepository.save(temporaryToken);
        return token;
    }

    public TemporaryToken getSubject (String token) {
        return temporaryTokenRepository.findByToken(token).orElseThrow(() -> new InternalErrorException("Token Error Parse"));
    }

    public boolean checkingTokenJwt(String token) {
        TemporaryToken tokenExits = temporaryTokenRepository.findByToken(token).orElse(null);
       if (tokenExits == null) {
           System.out.println("Token Kosong");
           return false;
       }
       if (tokenExits.getExpiredDate().before(new Date())) {
           System.out.println("Token Expired");
           return false;
       }
       return true;
}
}
