package com.crud.react.repository;

import com.crud.react.model.Users;
import org.apache.juli.logging.Log;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, Long> {
    Optional<Users> findByEmail(String email);

//    Boolean existsByEmail(String email);
//
//    void deleteById(Log id);
}
