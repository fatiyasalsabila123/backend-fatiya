package com.crud.react.exception;

import com.crud.react.exception.InternalErrorException;
import com.crud.react.response.ResponseHelper;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

public class GlobalException {

    @ExceptionHandler(ChangeSetPersister.NotFoundException.class)
    public ResponseEntity<?> notFound(ChangeSetPersister.NotFoundException notFoundException) {
        return ResponseHelper.error(notFoundException.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InternalErrorException.class)
    public ResponseEntity<?> internalEeror(InternalErrorException InternalErrorException) {
        return ResponseHelper.error(InternalErrorException.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
