package com.crud.react.DTO;

public class ProductDTO {


    private String namaMakanans;

    private String deskripsi;

    private Integer harga;


    public ProductDTO(String namaMakanans, String deskripsi, Integer harga) {
        this.namaMakanans = namaMakanans;
        this.deskripsi = deskripsi;
        this.harga = harga;
    }


    public String getNamaMakanans() {
        return namaMakanans;
    }

    public void setNamaMakanans(String namaMakanans) {
        this.namaMakanans = namaMakanans;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }
}
