package com.crud.react.model;


import javax.persistence.*;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;

    @Lob
    @Column(name = "gambar")
    private String gambar;

    @Column(name = "namaMakanans")
    private String namaMakanans;

    @Column(name = "deskripsi")
    private String deskripsi;

    @Column(name = "harga")
    private Integer harga;

    public Product() {
    }

    public Product(String gambar, String namaMakanans, String deskripsi, Integer harga) {
        this.gambar = gambar;
        this.namaMakanans = namaMakanans;
        this.deskripsi = deskripsi;
        this.harga = harga;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getNamaMakanans() {
        return namaMakanans;
    }

    public void setNamaMakanans(String namaMakanans) {
        this.namaMakanans = namaMakanans;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public Integer getHarga() {
        return harga;
    }

    public void setHarga(Integer harga) {
        this.harga = harga;
    }

    @Override
    public String toString() {
        return "Product{" +
                "Id=" + Id +
                ", gambar='" + gambar + '\'' +
                ", namaMakanans='" + namaMakanans + '\'' +
                ", deskripsi='" + deskripsi + '\'' +
                ", harga=" + harga +
                '}';
    }
}
